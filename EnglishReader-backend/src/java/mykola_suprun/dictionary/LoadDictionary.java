package mykola_suprun.dictionary;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class LoadDictionary {

    public static Dictionary deserializeDictionary(String file) {
        Dictionary dict = null;

        try {
            FileInputStream inputStream = new FileInputStream(file);
            ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
            dict = (Dictionary) objectInputStream.readObject();
            objectInputStream.close();
            inputStream.close();

        } catch (IOException i) {
            i.printStackTrace();

        } catch (ClassNotFoundException c) {
            System.out.println("Dictionary class not found");
            c.printStackTrace();

        }
        return dict;
    }
}
