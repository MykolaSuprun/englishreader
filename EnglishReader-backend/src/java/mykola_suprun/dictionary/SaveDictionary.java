package mykola_suprun.dictionary;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class SaveDictionary {

    public static void serializeDictionary(Dictionary dict, String file) {
        try {
            FileOutputStream outputStream = new FileOutputStream(file);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
            objectOutputStream.writeObject(dict);
            objectOutputStream.close();
            outputStream.close();

            System.out.println("Dictionary was serialized to: " + file);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

}
