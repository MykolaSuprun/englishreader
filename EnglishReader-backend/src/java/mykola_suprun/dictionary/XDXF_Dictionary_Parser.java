package mykola_suprun.dictionary;

import mykola_suprun.book_meta.FileToStringBuilder;

public class XDXF_Dictionary_Parser {

    final private static String keyOpen = "<k>";
    final private static String keyClose = "</k>";
    final private static String transOpen = "<ar>";
    final private static String transClose = "</ar>";
    final private static String transcriptionOpen = "<tr>";
    final private static String transcriptionClose = "</tr>";

    public static Dictionary parse(String file) {

        StringBuilder xml = FileToStringBuilder.getFile(file);
        xml.delete(0, xml.indexOf("</description>") + "</description>".length());

        Dictionary dict = new Dictionary();
        int i = 0, n = 0;

        while (xml.indexOf(transOpen) != -1) {
            if (i == 1000) {
                n += i;
                i = 0;
            }

            if (xml.indexOf(transcriptionOpen) != -1) {
                int tmp1 = xml.indexOf(transcriptionOpen);
                xml.delete(tmp1, tmp1 + transcriptionOpen.length());
                int tmp2 = xml.indexOf(transcriptionClose);
                xml.delete(tmp2, tmp2 + transcriptionClose.length());
                xml.insert(tmp1, "Transcriprion: ");
                xml.insert(tmp2 + "Transcriprion: ".length(), "\n");

            }
            String key = xml.substring(xml.indexOf(keyOpen) + keyOpen.length(), xml.indexOf(keyClose));
            String value = xml.substring((xml.indexOf(keyClose) + keyClose.length()), xml.indexOf(transClose));
            value = value.replaceAll("&quot;", "");
            value = value.substring(value.indexOf(' '));
            value = value.substring(0,1).toUpperCase() + value.substring(1);
            xml.delete(0, xml.indexOf(transClose) + transClose.length());

            dict.addEntry(key, value);
            i++;
            System.out.println(i);


            System.out.println("Stage: " + n);
            System.out.println("Word n: " + i);

        }


        System.out.println("loading finished");
        return dict;
    }
}
