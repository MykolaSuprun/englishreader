package mykola_suprun.book_meta;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Map;

public class Test {
    public static void main(String... args){

        TextProcessor text = new TextProcessor(FileToStringBuilder.getFile("/home/nikalyay/test/text.txt").toString());
        text.serializeLemmas("/home/nikalyay/test/lemmas.serialized");

    }
}
