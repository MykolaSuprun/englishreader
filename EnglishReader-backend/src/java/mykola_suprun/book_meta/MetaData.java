package mykola_suprun.book_meta;

import java.io.Serializable;
import java.util.HashMap;

public class MetaData implements Serializable {
    private String text;
    private HashMap<String,String> lemmas;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public HashMap<String, String> getLemmas() {
        return lemmas;
    }

    public void setLemmas(HashMap<String, String> lemmas) {
        this.lemmas = lemmas;
    }

   MetaData(MetaData data){
        this.lemmas = data.getLemmas();
        this.text = data.getText();
   }

    MetaData(HashMap<String,String> lemmas, String text){
        this.lemmas = lemmas;
        this.text = text;
    }
}
