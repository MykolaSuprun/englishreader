package mykola_suprun.book_meta;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class FileToStringBuilder {

    public static StringBuilder getFile(String file) {
        StringBuilder text = new StringBuilder();

        String line;

        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));

            while ((line = bufferedReader.readLine()) != null) {
                text.append(line).append("\n");
            }
            bufferedReader.close();
        } catch (FileNotFoundException e) {
            System.err.println("File not found");
        } catch (IOException ex) {
            System.err.println("Error reading file " + "'" + file + "'");
        }

        return text;
    }

}

