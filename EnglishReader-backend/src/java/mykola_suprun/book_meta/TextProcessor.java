package mykola_suprun.book_meta;

import edu.stanford.nlp.simple.*;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.HashMap;

public class TextProcessor {

    private StringBuilder text;

    public HashMap<String,String> getLemmas() {
        return lemmas;
    }

    private HashMap lemmas;

    public StringBuilder getText() {
        return text;
    }

    TextProcessor(String text) {
        String[] words = text.split("\\s+");

        HashMap<String, String> lemmasMap = new HashMap();

        for(int i = 0; i < words.length; i++)
            lemmasMap.put(words[i], new Sentence(words[i]).lemma(0));

        this.text = new StringBuilder(text);
        this.lemmas = lemmasMap;

    }

    public void saveMetaData(String file){
        try {
            FileOutputStream outputStream = new FileOutputStream(file);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
            objectOutputStream.writeObject(new MetaData(lemmas, text.toString()));
            objectOutputStream.close();
            outputStream.close();

            System.out.println("MetaData was serialized to: " + file);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public void serializeLemmas(String file){
        try {
            FileOutputStream outputStream = new FileOutputStream(file);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
            objectOutputStream.writeObject(lemmas);
            objectOutputStream.close();
            outputStream.close();

            System.out.println("Lemmas were serialized to: " + file);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

}
