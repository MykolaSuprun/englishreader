package mykola_suprun.dictionary;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;

public class Dictionary implements Serializable {
    HashMap<String, ArrayList<String>> dict = new HashMap<>();

    public HashMap getDictionary() {
        return dict;
    }

    public void setDictionary(HashMap<String, ArrayList<String>> dict) {
        this.getDictionary();
    }

    public void addEntry(String key, String value) {

        ArrayList<String> valueNow = dict.get(key);

        if (valueNow == null) {
            valueNow = new ArrayList<String>();
            dict.put(key, valueNow);
        }
        valueNow.add(value);

    }

    public Optional<ArrayList<String>> getEntry(String key) {

        return Optional.ofNullable(dict.get(key));

    }

}
