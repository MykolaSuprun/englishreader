package mykola_suprun;

import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.*;
import mykola_suprun.book_meta.LoadLemmas;
import mykola_suprun.dictionary.Dictionary;
import mykola_suprun.dictionary.LoadDictionary;
import org.vaadin.alump.labelbutton.LabelButton;
import sun.font.TrueTypeFont;

import java.util.*;

/**
 * This UI inputStream the application entry point. A UI may either represent a browser window
 * (or tab) or some part of an HTML page where a Vaadin application inputStream embedded.
 * <p>
 * The UI inputStream initialized using {@link #init(VaadinRequest)}. This method inputStream intended to be
 * overridden to add component to the user interface and initialize non-component functionality.
 */
@Theme("mytheme")
public class MyUI extends UI {

    private Dictionary dict = LoadDictionary.deserializeDictionary(this.getClass().getClassLoader().getResource("en-pl.serialized").getFile());
    private HashMap<String, String> lemmas = LoadLemmas.deserializeDictionary(this.getClass().getClassLoader().getResource("lemmas.serialized").getFile());
    private HashMap<String, ArrayList<Integer>> wordClickingData = new HashMap<>();
    //words we are checking for knowledge
    private List<String> wordsToCheck = new ArrayList();
    //answers to these words given by user
    private List<Boolean> wordsToCheckAndswers = new ArrayList<>();
    //lemmas which where found in the dictionary
    private HashMap<String, String> lemmasWithTranslations = new HashMap<>();
    //lemmas with answers to them from the wordsToCheckAnswers
    private HashMap<String, Boolean> lemmasWithAnswers = new HashMap<>();

    @Override
    protected void init(VaadinRequest vaadinRequest) {

        String text = FileToStringBuilder.getFile(this.getClass().getClassLoader().getResource("text.txt").getFile()).toString();
        String[] words = text.split("\\s+");


        for (Map.Entry<String, String> i : lemmas.entrySet())
            if (dict.getEntry(i.getValue()).isPresent())
                lemmasWithTranslations.put(i.getKey(), i.getValue() );


        Random generator = new Random();
        Object[] values = lemmasWithTranslations.keySet().toArray();


        for (int i = 0; i < 10; i++)
            wordsToCheck.add((String) values[generator.nextInt(values.length)]);


        final VerticalLayout wordChoosingMainLayout = new VerticalLayout();
        final HorizontalLayout wordRow = new HorizontalLayout();
        final HorizontalLayout answerRow = new HorizontalLayout();
        wordChoosingMainLayout.addComponent(wordRow);
        wordChoosingMainLayout.addComponent(answerRow);
        setContent(wordChoosingMainLayout);

        Label wordToCheck = new Label(wordsToCheck.get(0));
        wordRow.addComponent(wordToCheck);

        Button answer1 = new Button("Know");
        Button answer2 = new Button("Don't know");

        answer1.addClickListener(e -> {
            //filling the answers to the words
            if (wordsToCheckAndswers.size() < wordsToCheck.size()) {
                wordsToCheckAndswers.add(true);
                if (wordsToCheckAndswers.size() != wordsToCheck.size())
                    wordToCheck.setValue(lemmasWithTranslations.get(wordsToCheck.get(wordsToCheckAndswers.size())));
            } else {
                for (int i = 0; i < wordsToCheck.size(); i++) {
                    lemmasWithAnswers.put(lemmasWithTranslations.get(wordsToCheck.get(i)), wordsToCheckAndswers.get(i));
                }
                initContent(words);
            }

        });
        answer2.addClickListener(e -> {
            //filling the answers to the words
            if (wordsToCheckAndswers.size() < wordsToCheck.size()) {
                wordsToCheckAndswers.add(false);
                if (wordsToCheckAndswers.size() != wordsToCheck.size())
                    wordToCheck.setValue(lemmasWithTranslations.get(wordsToCheck.get(wordsToCheckAndswers.size())));
            } else {
                for (int i = 0; i < wordsToCheck.size(); i++) {
                    lemmasWithAnswers.put(lemmasWithTranslations.get(wordsToCheck.get(i)), wordsToCheckAndswers.get(i));
                }
                initContent(words);
            }
        });

        answerRow.addComponents(answer1, answer2);


    }

    @WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = MyUI.class, productionMode = false)
    public static class MyUIServlet extends VaadinServlet {
    }

    private void initContent(String[] words) {
        final HorizontalLayout layout = new HorizontalLayout();
        layout.setSpacing(true);

        Panel bookPanel = new Panel("Book Panel");
        bookPanel.setWidth("800px");
        bookPanel.setHeight("1000px");
        CssLayout bookLayout = new CssLayout();
        bookPanel.setContent(bookLayout);

        Panel dictionaryPanel = new Panel("Dictionary Panel");
        dictionaryPanel.setWidth("400px");
        VerticalLayout dictionaryLayot = new VerticalLayout();
        dictionaryPanel.setContent(dictionaryLayot);


        // add text to the book panel
        for (int i = 0; i < words.length; i++) {


            if (lemmasWithTranslations.containsKey(words[i]) && lemmasWithAnswers.containsKey(words[i])
            && !lemmasWithAnswers.get(lemmasWithTranslations.get(words[i]))) {

                    Label word = new Label(words[i]);
                    ComboBox wordTrans = new ComboBox();

                    ArrayList<String> trans = dict.getEntry(lemmasWithTranslations.get(words[i])).get();
                    for(int j = 0; j < trans.size(); j++){
                        String tmp = trans.get(j);
                        tmp = tmp.replaceAll("&quot;", "");
                        //if(tmp.indexOf(' ') != -1)
                        tmp = tmp.substring(tmp.indexOf(' ')+1);
                        tmp = tmp.substring(0,1).toUpperCase() + tmp.substring(1);
                        trans.set(j,tmp);
                    }
                    wordTrans.setItems(trans);
                    wordTrans.setValue(trans.get(0));



                    bookLayout.addComponent(word);
                    bookLayout.addComponent(new Label("&nbsp;", ContentMode.HTML));
                    bookLayout.addComponent(wordTrans);
                    bookLayout.addComponent(new Label("&nbsp;", ContentMode.HTML));

            } else if (dict.getEntry(lemmas.get(words[i])).isPresent()) {
                //if word is found in the dictionary add it as LabelButton

                LabelButton word = new LabelButton(words[i]);
                ArrayList<String> translations = dict.getEntry(lemmas.get(words[i])).get();

                //if word is clickable put word to the wordClickingData so it can be used to train perceptron
                wordClickingData.put(word.getValue(), new ArrayList<Integer>() {{
                    add(word.getValue().length());
                    add(0);
                }});

                //if word was clicked, put translation to the dictionary panel
                word.addLabelClickListener(e -> {
                    dictionaryLayot.removeAllComponents();

                    for (int j = 0; j < translations.size(); j++) {
                        String trans = translations.get(j).replaceAll("&quot;", "");
                        trans = trans.substring(trans.indexOf(' ') +1);
                        dictionaryLayot.addComponent(new Label(trans));
                    }

                    ArrayList<Integer> values = wordClickingData.get(word.getValue());
                    values.set(1, values.get(1) + 1);


//                    if(wordClickingData.size() > 10)
//                        for(Map.Entry<String,ArrayList<Integer>> it : wordClickingData.entrySet()){
//                            System.out.println(it.getKey() + " lenght is: " + it.getValue().get(0) +
//                                    " it was clicked " + it.getValue().get(1) + " times");
//
//
//                        }
                });

                bookLayout.addComponent(word);
                bookLayout.addComponent(new Label("&nbsp;", ContentMode.HTML));
            } else {
                //if there is no such word in the dictionary, add it as label
                bookLayout.addComponent(new Label(words[i]));
                bookLayout.addComponent(new Label("&nbsp;", ContentMode.HTML));
            }


        }

//        for (int i = 0; i < words.length; i++) {
//            bookLayout.addComponent(new LabelButton(
//                    String.format("<font size = \"4\" color=\"black\">%s", words[i])
//                    , ContentMode.HTML));
//            //cssLayout.addComponent(new Label("&nbsp;", ContentMode.HTML));
//        }


        layout.addComponents(bookPanel);
        layout.addComponents(dictionaryPanel);
        setContent(layout);
    }
}


//        Button button = new Button("Click Me");
//        button.addClickListener(e -> {
//            layout.addComponent(new Label("Thanks " + name.getValue()
//                    + ", it works!"));
//        });

//layout.addComponents(name, button);