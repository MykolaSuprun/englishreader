package mykola_suprun.book_meta;

import mykola_suprun.FileToStringBuilder;
import mykola_suprun.dictionary.Dictionary;
import mykola_suprun.dictionary.LoadDictionary;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.HashMap;
import java.util.Map;

public class Test {
    public static void main(String... args) {
        Dictionary dict = LoadDictionary.deserializeDictionary("/home/nikalyay/test/en-pl.serialized");
        HashMap<String,String> lemmas = LoadLemmas.deserializeDictionary("/home/nikalyay/test/lemmas.serialized");
        String text = FileToStringBuilder.getFile("/home/nikalyay/test/text.txt").toString();

      //  System.out.println(text);

       System.out.println(dict.getEntry(lemmas.get("been")).get());

    }
}
