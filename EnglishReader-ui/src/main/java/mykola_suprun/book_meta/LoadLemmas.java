package mykola_suprun.book_meta;

import mykola_suprun.dictionary.Dictionary;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.HashMap;

public class LoadLemmas {
    public static HashMap<String,String> deserializeDictionary(String file) {
        HashMap<String,String> lemmas = null;

        try {
            FileInputStream inputStream = new FileInputStream(file);
            ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
            lemmas = (HashMap<String, String>) objectInputStream.readObject();
            objectInputStream.close();
            inputStream.close();

        } catch (IOException i) {
            i.printStackTrace();

        } catch (ClassNotFoundException c) {
            System.out.println("Dictionary class not found");
            c.printStackTrace();

        }
        return lemmas;
    }
}
